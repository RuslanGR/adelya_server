from rest_framework.generics import CreateAPIView, ListCreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import (
    UserSerializer,
    MedicationSerializer,
    OrderItemSerializer,
)
from .models import (
    User,
    Medication,
    Order,
    OrderItem,
)


class RegistrationView(CreateAPIView):

    permission_classes = (AllowAny,)
    serializer_class = UserSerializer


class MedicationList(ListCreateAPIView):

    permission_classes = (AllowAny,)
    serializer_class = MedicationSerializer
    queryset = Medication.objects.all()


class OrderAPIView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, _, pk):
        user = User.objects.get(pk=pk)
        order, _ = Order.objects.get_or_create(user=user)
        items = order.orderitem_set.all()
        serialized_items = OrderItemSerializer(items, many=True)

        return Response(serialized_items.data)

    def post(self, request, pk):
        user = User.objects.get(pk=pk)
        order, _ = Order.objects.get_or_create(user=user)
        data = request.data
        medication = Medication.objects.get(pk=data['id'])
        order_item, _ = OrderItem.objects.get_or_create(medication=medication, order=order)

        if data['type'] == 'ADD':
            order_item.count = order_item.count + 1
            order_item.save()
        if data['type'] == 'DELETE':
            if order_item.count > 1:
                order_item.count = order_item.count - 1
                order_item.save()
            elif order_item.count == 1:
                order_item.delete()

        return Response({})
