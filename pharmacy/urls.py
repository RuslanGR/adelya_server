from django.urls import path

from .views import *


urlpatterns = (
    path('medication/', MedicationList.as_view()),
    path('order/<int:pk>/', OrderAPIView.as_view()),
)
