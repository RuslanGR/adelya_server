from django.db import models
from django.contrib.auth.models import User


class Medication(models.Model):

    name = models.CharField(max_length=128)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return self.name


class Order(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    paid = models.BooleanField(verbose_name='Заказ оплачен', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


class OrderItem(models.Model):

    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    medication = models.ForeignKey(Medication, on_delete=models.CASCADE)
    count = models.PositiveIntegerField(default=0)
