from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer

from .models import (
    Medication,
    Order,
    OrderItem,
)


class UserSerializer(ModelSerializer):
    """User serializer"""

    class Meta:
        """Meta"""

        model = User
        fields = ('username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """Register new user"""
        print('heh')
        user = User(
            username=validated_data.get('username'),
            email=validated_data.get('email')
        )
        user.set_password(validated_data.get('password'))
        user.save()
        return user


class MedicationSerializer(ModelSerializer):
    class Meta:

        model = Medication
        fields = '__all__'


class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(ModelSerializer):
    medication = MedicationSerializer()

    class Meta:
        model = OrderItem
        fields = '__all__'
