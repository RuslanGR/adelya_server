from django.contrib import admin

from .models import (
    Medication,
    Order,
    OrderItem,
)


admin.site.register(Medication)
admin.site.register(Order)
admin.site.register(OrderItem)
